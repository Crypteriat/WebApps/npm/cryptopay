export interface payButton {
  ticker: string;
  addressOrLink: string | Object;
  setAmount: Function;
  setEmail: Function;
  setQRMessage: Function;
  color?: string;
  invoice: boolean;
  invoiceOptions?: any;
}

export interface cryptoPay {
  addresses: (
    | {
        ticker: string;
        payserver: string;
        address?: undefined;
        link?: undefined;
      }
    | {
        ticker: string;
        address: string;
        payserver?: undefined;
        link?: undefined;
      }
    | {
        ticker: string;
        link: string;
        payserver?: undefined;
        address?: undefined;
      }
  )[];
  invoice: boolean;
  lbtcOptions: { QRMessage: string };
  btcOptions: {
    buttonDes: string;
    tooltipDes: string;
    fieldDes: string;
    QRMessage: string;
  };
  ethOptions: {
    buttonDes: string;
    tooltipDes: string;
    fieldDes: string;
    QRMessage: string;
  };
  bnbOptions: {
    buttonDes: string;
    tooltipDes: string;
    fieldDes: string;
    QRMessage: string;
  };
  paypalOptions: {
    fieldDes: string;
  };
}

export interface cryptoQRCard {
  address: string;
  ticker: string;
  size?: number;
  email: string;
  amount: string;
  message: string;
}

export interface selCryptoButton {
  tickersParam: Array<string>;
  color: string;
  tickerSelect: Function;
}
