import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { MdContentCopy } from 'react-icons/md/index.js';
import ReactTooltip from 'react-tooltip';

export default function CopyButton({ addressOrLink, copyState }) {
  const copyButton = (
    <span>
      <ReactTooltip />
      <CopyToClipboard
        text={addressOrLink}
        data-tip={copyState ? 'Copied' : 'Copy Address to Clipboard'}
        data-text-color="yellow"
        data-border="true"
      >
        <span className="inline-flex flex-nowrap">
          {copyState ? 'Copied' : 'Copy'} &nbsp; <MdContentCopy />
        </span>
      </CopyToClipboard>
    </span>
  );

  return copyButton;
}
