import SelCryptoButtons from './SelCryptoButtons';
import PayButtons from './PayButtons';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import CryptoQRCard from './CryptoQRCard';
import { cryptoPay } from '../lib/types';

const ReactTooltip = dynamic(() => import('react-tooltip'), {
  ssr: false,
});

export default function CryptoPay({
  addresses,
  invoice = false,
  lbtcOptions = {
    QRMessage: '',
  },
  btcOptions = {
    buttonDes: '',
    tooltipDes: '',
    fieldDes: '',
    QRMessage: '',
  },
  ethOptions = {
    buttonDes: '',
    tooltipDes: '',
    fieldDes: '',
    QRMessage: '',
  },
  bnbOptions = {
    buttonDes: '',
    tooltipDes: '',
    fieldDes: '',
    QRMessage: '',
  },
  paypalOptions = {
    fieldDes: '',
  },
}: cryptoPay) {
  //TODO default address
  const [tickerSymbol, tickerSelect] = useState(addresses[0].ticker);
  const [metadata, setMetadata] = useState(
    addresses[0].link ? addresses[0].link : addresses[0].address
  );
  const [amount, setAmount] = useState('');
  const [email, setEmail] = useState('');
  const [message, setQRMessage] = useState('');

  useEffect(() => {
    const foundElement = addresses.find(
      (element) => element.ticker.toLowerCase() === tickerSymbol.toLowerCase()
    );
    setMetadata(
      foundElement.link ||
        foundElement.address ||
        foundElement.payserver ||
        addresses[0].link ||
        addresses[0].address
    );
  }, [tickerSymbol]);

  let tickers = [];
  for (let i = 0; i < addresses.length; i++) {
    tickers.push(addresses[i].ticker.toUpperCase());
  }

  const invoiceOptions = {
    lbtc: lbtcOptions,
    btc: btcOptions,
    eth: ethOptions,
    bnb: bnbOptions,
    paypal: paypalOptions,
  };

  return (
    <>
      <div className="flex flex-col items-center text-gray-300 mt-7 dark:text-gray-400">
        <ReactTooltip />
        <SelCryptoButtons
          tickersParam={tickers}
          tickerSelect={tickerSelect}
          color="bg-blue-400 dark:bg-blue-500"
        />
        <span className="pt-3 mx-auto text-gray-700">
          <CryptoQRCard
            message={message}
            amount={amount}
            email={email}
            ticker={tickerSymbol}
            address={metadata}
          />
        </span>
        <PayButtons
          ticker={tickerSymbol}
          setAmount={setAmount}
          setEmail={setEmail}
          setQRMessage={setQRMessage}
          addressOrLink={metadata}
          invoice={invoice}
          invoiceOptions={invoiceOptions}
        />
      </div>
    </>
  );
}

CryptoPay.propTypes = {
  addresses: PropTypes.array.isRequired,
};
