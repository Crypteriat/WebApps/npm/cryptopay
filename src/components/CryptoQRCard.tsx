/* eslint-disable indent */
import Link from './Link';
import QRCode from 'react-qr-code';
import PropTypes from 'prop-types';
import { cryptoQRCard } from 'src/lib/types';
function CryptoQRCard({
  address,
  ticker,
  size = 350,
  email,
  amount,
  message,
}: cryptoQRCard) {
  let coin: string;
  let addressLink: string;
  if (ticker) {
    switch (ticker.toLowerCase()) {
      case 'btc':
        coin = 'bitcoin';
        addressLink = `https://blockchain.com/btc/address/${address}`;
        break;

      case 'eth':
        coin = 'ethereum';
        addressLink = `https://etherscan.io/address/${address}`;
        break;

      case 'lbtc':
        coin = '...not applicable...';
        addressLink = '';
        break;

      case 'bnb':
        coin = 'binancecoin';
        addressLink = `https://explorer.binance.org/address/${address}`;
        break;

      case 'payppal':
        coin = 'paypal';
        addressLink = address;
        break;

      default:
        break;
    }
  }

  // Format for value: bitcoin:<address>[?amount=<amount>][?label=<label>][?message=<message>]
  const coinNAddress = `${coin}:${address}`;
  const amountparam = amount ? `?amount=${amount}` : '';
  const labelparam = email ? `?label=${email}` : '';
  const cryptoMessage = email ? `?message=${email}` : '';

  const uri = `${coinNAddress}${amountparam}${labelparam}${cryptoMessage}`;
  const encuri = encodeURI(uri);

  return (
    <>
      {message ? (
        <span className="flex flex-col h-64 max-w-md text-2xl text-center text-gray-300 justify-evenly lg:h-96 lg:text-3xl dark:text-gray-500">
          {message}
        </span>
      ) : (
        <span className="flex flex-col justify-center object-scale-down">
          <Link href={addressLink} aria-label={`Link to ${addressLink}`}>
            <QRCode size={size} value={encuri} level="Q" />
          </Link>
        </span>
      )}
    </>
  );
}

CryptoQRCard.propTypes = {
  address: PropTypes.string,
  ticker: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  email: PropTypes.string.isRequired,
  amount: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired,
  ]),
  message: PropTypes.string,
};
export default CryptoQRCard;
