export default function Form(props) {
  const value = (event) => {
    event.preventDefault(); // Don't redirect the page
    // where we'll add our form logic
  };

  const { name, action, ...other } = props;
  return (
    <form onSubmit={value}>
      <label {...other}>{name}</label>
      <input {...other} required />
      <button {...other}>{action}</button>
    </form>
  );
}
