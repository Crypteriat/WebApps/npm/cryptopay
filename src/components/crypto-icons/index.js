import React from 'react'
import Bitcoin from './bitcoin.svg'
import Ethereum from './ethereum.svg'
import Paypal from './paypal.svg'
import LightningBitcoin from './lightning_bitcoin.svg'
import Binance from './binance_coin.svg'
import PropTypes from 'prop-types'

const components = {
  BTC: Bitcoin,
  LBTC: LightningBitcoin,
  ETH: Ethereum,
  PAYPAL: Paypal,
  BNB: Binance,
}

const CryptoIcons = ({ kind, size = 5 }) => {
  const CryptoSvg = components[kind]

  return (
    <span className="text-sm text-gray-500 transition hover:text-gray-600">
      <span className="sr-only">{kind}</span>
      <CryptoSvg
        className={`fill-current hover:text-blue-500 dark:hover:text-blue-400 h-${size} w-${size}`}
      />
    </span>
  )
}

CryptoIcons.propTypes = {
  kind: PropTypes.string.isRequired,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
}

export default CryptoIcons
