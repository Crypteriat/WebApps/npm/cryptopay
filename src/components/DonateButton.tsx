import React from 'react';
import { FaFileInvoice } from 'react-icons/fa/index.js';
import ReactTooltip from 'react-tooltip';

export default function DonateButton({ dataTip, description }) {
  const donateButton = (
    <span>
      <ReactTooltip />
      <span
        className="inline-flex flex-nowrap"
        data-tip={
          dataTip
            ? dataTip
            : 'Create and Email Custom Donation Amount with Receipt'
        }
        data-text-color="yellow"
        data-border="true"
      >
        {description ? description : 'Donation Receipt'}&nbsp; <FaFileInvoice />
      </span>
    </span>
  );

  return donateButton;
}
