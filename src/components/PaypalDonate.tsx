import React from 'react';
import Link from './Link';
import PaypalImage from '../static/paypal_donate_lg.png';

export default function PaypalDonate({ description, paypalLink }) {
  const paypalDonate = (
    <>
      <span className="pt-3 mt-2">
        <Link
          href={paypalLink}
          aria-label={`Link to PayPal: ${paypalLink}`}
          target="_blank"
          rel="noreferrer"
        >
          <img
            src={PaypalImage}
            alt={'PayPal'}
            className="object-center"
            width={120}
            height={33}
          />
        </Link>
      </span>

      <span className="text-gray-700 dark:text-gray-300 mt-2">
        {description
          ? description
          : 'You will be redirected to a PayPal payment website for: Only a Test'}
      </span>
    </>
  );

  return paypalDonate;
}
