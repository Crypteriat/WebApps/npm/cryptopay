/* eslint-disable indent */
import React, { useState, useEffect } from 'react';
import { InvoiceCryptoInfo, CopyCryptoInfo } from './CryptoInfo';
import ReactTooltip from 'react-tooltip';
import { payButton } from '../lib/types';

export default function PayButtons({
  ticker,
  addressOrLink,
  setAmount,
  setEmail,
  setQRMessage,
  color = 'green',
  invoice,
  invoiceOptions,
}: payButton) {
  const [openButton, setopenButton] = React.useState(0);
  const [fieldSelection, setFieldSelection] = useState(0);
  const [copyColor, setCopyColor] = useState(color);
  const [copyState, setCopied] = useState(false);

  let tabs: Array<Object>;
  let fields: Array<Object>;

  useEffect(() => {
    setCopied(false);
    setCopyColor(color);
    setFieldSelection(0);
    setopenButton(0);
  }, [ticker]);

  useEffect(() => {
    const timer = () =>
      setTimeout(() => {
        ReactTooltip.rebuild();
        setCopyColor(color);
        setCopied(false);
      }, 3500);
    const timerID = timer();
    return () => {
      clearTimeout(timerID);
    };
  }, [copyState]);

  function handleCopy() {
    setCopyColor('yellow');
    setCopied(true);
  }

  const invoiceInfo = InvoiceCryptoInfo(invoiceOptions, addressOrLink);
  const copyInfo = CopyCryptoInfo(addressOrLink, copyState, ticker);

  switch (ticker.toLowerCase()) {
    case 'lbtc':
      tabs = [];
      fields = [invoiceInfo.fields.lbtc];
      break;
    case 'btc':
      tabs = invoice
        ? [copyInfo.button, invoiceInfo.buttons.btc]
        : [copyInfo.button];
      fields = [copyInfo.field, invoiceInfo.fields.btc];
      break;
    case 'eth':
      tabs = invoice
        ? [copyInfo.button, invoiceInfo.buttons.eth]
        : [copyInfo.button];
      fields = [copyInfo.field, invoiceInfo.fields.eth];
      break;
    case 'bnb':
      tabs = invoice
        ? [copyInfo.button, invoiceInfo.buttons.bnb]
        : [copyInfo.button];
      fields = [copyInfo.field, invoiceInfo.fields.bnb];
      break;
    case 'paypal':
      tabs = [];
      fields = [invoiceInfo.fields.paypal];
      break;
    default:
      break;
  }

  const QRMessages = {
    lbtc: invoiceOptions.lbtc.QRMessage
      ? invoiceOptions.lbtc.QRMessage
      : 'Lightning Bitcoin requires generation of an invoice. Please input your email address and verify with link sent to you. When email is verified, please enter amount and a QR code will be created.',
    btc: invoiceOptions.btc.QRMessage
      ? invoiceOptions.btc.QRMessage
      : 'Please input your email address and verify with link sent to you. When email is verified, please enter amount and a QR code will be created.',
    eth: invoiceOptions.eth.QRMessage
      ? invoiceOptions.eth.QRMessage
      : 'An invoice will be created when an amount and email have been sumbitted. If no funds have been received within 48 hours, the invoice will expire.',
    bnb: invoiceOptions.bnb.QRMessage
      ? invoiceOptions.bnb.QRMessage
      : 'An invoice will be created when an amount and email have been sumbitted. If no funds have been received within 48 hours, the invoice will expire.',
  };

  useEffect(() => {
    if (ticker.toLowerCase() === 'lbtc') {
      setQRMessage(QRMessages.lbtc);
    } else if (ticker.toLowerCase() === 'btc' && openButton === 1) {
      setQRMessage(QRMessages.btc);
    } else if (ticker.toLowerCase() === 'paypal') {
      setQRMessage('');
    } else if (openButton === 0) {
      setQRMessage('');
    } else if (ticker.toLowerCase() === 'eth') {
      setQRMessage(QRMessages.eth);
    } else if (ticker.toLowerCase() === 'bnb'){
      setQRMessage(QRMessages.bnb);
    }
  }, [ticker, openButton]);

  return (
    <>
      {fields[fieldSelection]}
      <ul className="flex flex-row pt-1 mb-3 list-none gap-1" role="tablist">
        {tabs.map((button, i) => (
          <li
            key={`paybutton-${i}`}
            className="text-center first:ml-0 last:mr-0"
          >
            <a
              className={
                'text-xs block first:ml-0 last:mr-0 font-bold uppercase px-4 py-1 shadow-lg rounded leading-normal ' +
                (openButton === i
                  ? 'text-white bg-' + copyColor + '-600'
                  : 'text-' + color + '-600 bg-indigo-600')
              }
              onClick={(event) => {
                event.preventDefault();
                setopenButton(i);
                setFieldSelection(i);
                if (fieldSelection === 0) {
                  handleCopy();
                }
                if (openButton !== i) {
                  setCopied(false);
                  setCopyColor(color);
                }
              }}
              data-toggle="button"
              href="#link{i}"
              role="tablist"
            >
              <i className="mr-1 text-base">{button}</i>
            </a>
          </li>
        ))}
      </ul>
    </>
  );
}
