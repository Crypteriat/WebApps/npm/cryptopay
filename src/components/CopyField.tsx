import React from 'react';

export default function CopyField({ ticker, addressOrLink }) {
  const copyFields = (
    <span className="text-xs text-gray-700 dark:text-gray-300 mt-3 mb-3">
      {ticker}: {addressOrLink}
    </span>
  );

  return copyFields;
}
