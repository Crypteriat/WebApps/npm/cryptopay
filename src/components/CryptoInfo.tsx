import CopyButton from './CopyButton';
import CopyField from './CopyField';
import DonateButton from './DonateButton';
import DonateField from './DonateField';
import PaypalDonate from './PaypalDonate';
import PayServer from './PayServer';

export function InvoiceCryptoInfo(invoiceOptions, paypalLink) {
  const buttons = {
    btc: (
      <DonateButton
        key={1}
        dataTip={invoiceOptions.btc.tooltipDes}
        description={invoiceOptions.btc.buttonDes}
      />
    ),
    eth: (
      <DonateButton
        key={1}
        dataTip={invoiceOptions.eth.tooltipDes}
        description={invoiceOptions.eth.buttonDes}
      />
    ),
    bnb: (
      <DonateButton
        key={1}
        dataTip={invoiceOptions.bnb.tooltipDes}
        description={invoiceOptions.bnb.buttonDes}
      />
    ),
  };

  const fields = {
    lbtc: <PayServer key={0} />,
    btc: <DonateField key={0} description={invoiceOptions.btc.fieldDes} />,
    eth: <DonateField key={0} description={invoiceOptions.eth.fieldDes} />,
    bnb: <DonateField key={0} description={invoiceOptions.bnb.fieldDes} />,
    paypal: (
      <PaypalDonate
        key={0}
        description={invoiceOptions.paypal.fieldDes}
        paypalLink={paypalLink}
      />
    ),
  };

  return { buttons, fields };
}

export function CopyCryptoInfo(
  addressOrLink: Object | String,
  copyState: boolean,
  ticker: string
) {
  const button = (
    <CopyButton key={0} addressOrLink={addressOrLink} copyState={copyState} />
  );
  const field = (
    <CopyField key={0} ticker={ticker} addressOrLink={addressOrLink} />
  );

  return { button, field };
}
