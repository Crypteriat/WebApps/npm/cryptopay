import React from 'react';
import Form from './Form';

export default function DonateField({ description }) {
  const donateFields = (
    <span className="flex flex-col items-center text-gray-700 gap-1 dark:text-gray-300">
      <span className="inline-flex items-baseline h-5 ml-1 text-sm flex-nowrap">
        {'Amount: '}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <Form
          className="bg-gray-200 dark:bg-gray-700"
          placeholder="               $USD"
        />
      </span>
      <span className="inline-flex h-5 text-sm flex-nowrap">
        {'Email Address: '}&nbsp; &nbsp;
        <Form
          className="bg-gray-200 dark:bg-gray-700"
          placeholder="    you@youremail.com"
        />
      </span>
      <span className="flex flex-col items-center mt-1">
        <button className="inset-x-0 bottom-0 px-2 h-6 text-gray-100 bg-indigo-500 rounded-md hover:bg-indigo-700">
          {'Submit'}
        </button>
      </span>
      <p className="inline-flex mt-2 text-center sm:mx-14 md:mx-7">
        {description
          ? description
          : 'After submitting you will receive a custom QR code for your donation.'}
      </p>
    </span>
  );

  return donateFields;
}
