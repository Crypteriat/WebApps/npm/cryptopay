const path = require('path')
const pkg = require('./package.json')
const nodeExternals = require('webpack-node-externals')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: './src/index.ts',
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: '[name][ext][query]',
    filename: 'bundle.js',
    clean: true,
    globalObject: 'this',
    umdNamedDefine: true,
    library: {
      name: pkg.name,
      type: 'umd',
    },
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'bundle.css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
        ],
      },
      {
        test: /\.png/,
        type: 'asset',
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  resolve: {
    alias: {
      // Needed when library is linked via `npm link` to app
      react: path.resolve('./node_modules/react'),
    },
    extensions: ['.tsx', '.ts', '.js', '.png'],
  },
  mode: 'production',
  target: 'node',
  externals: [nodeExternals()],
}
