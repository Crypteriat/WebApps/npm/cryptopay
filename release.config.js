module.exports = {
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/npm',
    [
      '@semantic-release/gitlab',
      {
        gitlabUrl: 'https://gitlab.com/Crypteriat/WebApps/npm/cryptopay',
        assets: [{ path: 'dist/bundle.js', label: 'JS distribution' }],
      },
    ],
  ],
}
