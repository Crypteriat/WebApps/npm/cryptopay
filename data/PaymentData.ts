export const paymentData = {
  btc: 'your btc address',
  lbtc: { apiKey: process.env.lbtcAPIkey, uri: process.env.ltbtcURI },
  eth: 'your eth address',
  bnb: 'your bnb address',
  paypal: 'your paypal link',
};
