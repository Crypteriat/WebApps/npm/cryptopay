# CryptoPay

A react component for payment in various cryptocurrencies including LBTC and Paypal

Currently the supported payments are:

- Lightning Bitcoin
- Bitcoin
- Ethereum
- Binance Coin
- Paypal

## Installation

```
npm install @crypteriat/cryptopay
```

## Use

```
import { CryptoPay } from '@crypteriat/cryptopay';
import '@crypteriat/cryptopay/dist/bundle.css';
import { paymentData } from '@data/PaymentData';

const addresses = [
    { ticker: 'lbtc', payserver: paymentData.lbtc.uri },
    { ticker: 'btc', address: paymentData.btc },
    { ticker: 'eth', address: paymentData.eth },
    { ticker: 'bnb', address: paymentData.bnb },
    { ticker: 'paypal', link: paymentData.paypal },
  ];

 <CryptoPay addresses={addresses} />
```

For now, make sure to also import the `bundle.css` file

<br>

Addresses are required. Not all have to be used. LBTC URI must be under 'payserver' and Paypal link must be under 'link'

- Addresses can take in your addresses from a separate file that might look something like:

```
export const paymentData = {
  btc: 'btc address',
  lbtc: { apiKey: process.env.lbtcAPIkey, uri: process.env.ltbtcURI },
  eth: 'eth address',
  bnb: 'bnb address',
  paypal: 'paypal link',
};
```

Rearranging the order of tickers will rearrange the order in which they are displayed in the tabs

## Options

| Global  | Type    | Default | Description                                  |
| :------ | :------ | :------ | :------------------------------------------- |
| invoice | boolean | false   | Display invoice button and field information |

Example:

```
<CryptoPay addresses={addresses} invoice={true} />
```

### INVOICE must be true for the following:

### - lbtcOptions

| Global    | Type   | Default     | Description                 |
| :-------- | :----- | :---------- | :-------------------------- |
| QRMessage | string | See example | Modfiy LBTC display message |

Example:

```
const lbtcOptions = {
    QRMessage: 'This is a lbtc test',
  };

<CryptoPay
  addresses={addresses}
  invoice={true}
  lbtcOptions={lbtcOptions}
/>
```

### - btcOptions, ethOptions, bnbOptions

| Global     | Type   | Default     | Description                                               |
| :--------- | :----- | :---------- | :-------------------------------------------------------- |
| buttonDes  | string | See example | Modfiy invoice button description                         |
| tooltipDes | string | See example | Modfiy invoice button tool-tip description                |
| fieldDes   | string | See example | Modfiy field description below 'amount' and 'email' forms |
| QRMessage  | string | See example | Modfiy display message                                    |

Example:

```
const btcOptions = {   //or ethOptions, bnbOptions
    buttonDes: 'This is a btc test',
    tooltipDes: 'This is a btc test',
    fieldDes: 'This is a btc test',
    QRMessage: 'This is a btc test',
  };

<CryptoPay
  addresses={addresses}
  invoice={true}
  btcOptions={btcOptions}
  ethOptions={ethOptions}
  bnbOptions={bnbOptions}
/>
```

### - paypalOptions

| Global   | Type   | Default     | Description                                    |
| :------- | :----- | :---------- | :--------------------------------------------- |
| fieldDes | string | See example | Modfiy field description below 'donate' button |

Example:

```
const paypalOptions = {
    fieldDes: 'This is a paypal test',
  };

<CryptoPay
  addresses={addresses}
  invoice={true}
  paypalOptions={paypalOptions}
/>
```

## Example

Clone the repo and run these commands:

```
npm i
npm run dev
```

## Notice

As said above, the only supported payment types are currently listed. Not all of them have to be used, but there is not capability right now to add any further options. We are still in progress.
