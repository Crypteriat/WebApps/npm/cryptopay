import React from 'react';
import Head from 'next/head.js';
import '../css/tailwind.css';

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>

      <Component {...pageProps} />
    </>
  );
}
