import React from 'react';
import { paymentData } from '../data/PaymentData';
import CryptoPay from '../src/components/CryptoPay';
import ThemeSwitch from './components/ThemeSwitch';

export default function Home() {
  const addresses = [
    { ticker: 'lbtc', payserver: paymentData.lbtc.uri },
    { ticker: 'btc', address: paymentData.btc },
    { ticker: 'eth', address: paymentData.eth },
    { ticker: 'bnb', address: paymentData.bnb },
    { ticker: 'paypal', link: paymentData.paypal },
  ];

  const lbtcOptions = {
    QRMessage: 'This is a lbtc test',
  };

  const btcOptions = {
    buttonDes: 'This is a btc test',
    tooltipDes: 'This is a btc test',
    fieldDes: 'This is a btc test',
    QRMessage: 'This is a btc test',
  };

  const ethOptions = {
    buttonDes: 'This is a eth test',
    tooltipDes: 'This is a eth test',
    fieldDes: 'This is a eth test',
    QRMessage: 'This is a eth test',
  };

  const bnbOptions = {
    buttonDes: 'This is a bnb test',
    tooltipDes: 'This is a bnb test',
    fieldDes: 'This is a bnb test',
    QRMessage: 'This is a bnb test',
  };

  const paypalOptions = {
    fieldDes: 'This is a paypal test',
  };

  return (
    <div className="flex flex-col items-center">
      <div className="mb-4 mt-5">
        <ThemeSwitch />
      </div>
      <CryptoPay
        addresses={addresses}
        invoice={true}
        lbtcOptions={lbtcOptions}
        btcOptions={btcOptions}
        ethOptions={ethOptions}
        bnbOptions={bnbOptions}
        paypalOptions={paypalOptions}
      />
    </div>
  );
}
