module.exports = {
  sourceType: 'unambiguous',
  presets: [
    [
      '@babel/preset-react',
      {
        targets: {
          node: 'current',
        },
        runtime: 'automatic',
      },
    ],
    [
      '@babel/preset-env',
      {
        corejs: '3.20.3',
        useBuiltIns: 'usage',
      },
    ],
    '@babel/preset-typescript',
  ],
}
